package com.attitudetech.basicstruct.main;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.attitudetech.basicstruct.R;

public class MainActivity extends AppCompatActivity {

    MainPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        presenter = new MainPresenter();

        presenter.getUser();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.disposal();
    }
}
