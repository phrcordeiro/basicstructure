package com.attitudetech.basicstruct.connection.user;

import com.attitudetech.basicstruct.connection.api.ApiClient;
import com.attitudetech.basicstruct.connection.api.requester.UserApiRequester;

import io.reactivex.Flowable;
import io.reactivex.Observable;

/**
 * Created by phrc on 2/7/17.
 */

public class UserApi implements IUser {

    @Override
    public Flowable user(String user) {
        return ApiClient
                .getClient()
                .create(UserApiRequester.class)
                .user(user);
    }
}
