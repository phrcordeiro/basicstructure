package com.attitudetech.basicstruct.main;

import android.util.Log;

import com.attitudetech.basicstruct.connection.socketio.SocketManager;
import com.attitudetech.basicstruct.datamanager.UserDataManager;
import com.attitudetech.basicstruct.pojo.User;

import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

import io.reactivex.Flowable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.subscribers.DefaultSubscriber;
import io.reactivex.subscribers.DisposableSubscriber;

/**
 * Created by phrc on 25/01/17.
 */

public class MainPresenter {

    UserDataManager userDataManager;

    public MainPresenter() {
        userDataManager = new UserDataManager();
    }

    public void getUser(){

        DisposableSubscriber<User> disposableSubscriber = new DisposableSubscriber<User>() {
            @Override
            public void onNext(User user) {
                Log.v("Disposal", "1");
            }

            @Override
            public void onError(Throwable t) {

            }

            @Override
            public void onComplete() {

            }
        };

        DefaultSubscriber<User> disposableSubscriber2 = new DefaultSubscriber<User>() {
            @Override
            public void onNext(User user) {
                Log.v("Disposal", "2");
            }

            @Override
            public void onError(Throwable t) {

            }

            @Override
            public void onComplete() {

            }
        };

        userDataManager.getUserSocket("new message", disposableSubscriber);
//        userDataManager.getUserAPI("cordeiroph", disposableSubscriber);
//        userDataManager.getUserAPI("cordeiroph", disposableSubscriber2);


        userDataManager.getUser("new messade", new DisposableSubscriber<User>() {
            @Override
            public void onNext(User user) {

            }

            @Override
            public void onError(Throwable t) {

            }

            @Override
            public void onComplete() {

            }
        });

    }

    public void disposal(){
        userDataManager.close();
    }


}
