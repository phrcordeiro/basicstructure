package com.attitudetech.basicstruct.connection.socketio;

import com.attitudetech.basicstruct.pojo.User;

import java.net.URISyntaxException;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.FlowableEmitter;
import io.reactivex.FlowableOnSubscribe;
import io.reactivex.functions.Action;
import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

/**
 * Created by phrc on 2/7/17.
 */

public class SocketManager {

    private String CHAT_SERVER_URL = "http://chat.socket.io";
    private Socket socket;
    private static SocketManager instance;


    private SocketManager() {
        try {
            socket = IO.socket(CHAT_SERVER_URL);
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    public static SocketManager instance() {
        if(instance == null){
            instance = new SocketManager();
        }
        return instance;
    }

    public SocketManager on(Emitter.Listener listener, String channel){
        socket.on(channel, listener);
        return this;
    }

    public Flowable on(String channel, SocketListener listener){
        return Flowable
                .create((FlowableEmitter<User> emmiter) -> {
                    listener.setEmmiter(emmiter);
                    SocketManager.instance().on(listener, channel).connect();
                }, BackpressureStrategy.BUFFER)
                .doOnCancel(new Action() {
                    @Override
                    public void run() throws Exception {
                        SocketManager.instance().off(channel, listener);
                    }
                });
    }

    public SocketManager off(String channel, Emitter.Listener listener){
        socket.off(channel, listener);
        return this;
    }

    public SocketManager connect(){
        if(!socket.connected()){
            socket.connect();
        }
        return this;
    }




}
