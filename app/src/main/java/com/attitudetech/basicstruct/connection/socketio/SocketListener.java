package com.attitudetech.basicstruct.connection.socketio;

import com.attitudetech.basicstruct.pojo.User;

import io.reactivex.FlowableEmitter;
import io.socket.emitter.Emitter;

/**
 * Created by phrc on 2/8/17.
 */

public class SocketListener<T> implements Emitter.Listener {

    FlowableEmitter<T> emmiter;

    public SocketListener() {
    }

    public void setEmmiter(FlowableEmitter<T> emmiter) {
        this.emmiter = emmiter;
    }

    @Override
    public void call(Object... args) {
        emmiter.onNext(convert(args));
    }

    protected T convert(Object object){
        return null;
    }
}
