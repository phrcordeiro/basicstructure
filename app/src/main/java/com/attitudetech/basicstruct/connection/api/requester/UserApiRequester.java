package com.attitudetech.basicstruct.connection.api.requester;

import com.attitudetech.basicstruct.pojo.User;

import io.reactivex.Flowable;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by phrc on 2/7/17.
 */

public interface UserApiRequester {

    @GET("/users/{user}")
    Flowable<User> user(@Path("user") String user);

}
