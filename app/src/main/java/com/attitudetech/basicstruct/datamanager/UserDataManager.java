package com.attitudetech.basicstruct.datamanager;


import com.attitudetech.basicstruct.connection.user.IUser;
import com.attitudetech.basicstruct.connection.user.UserApi;
import com.attitudetech.basicstruct.connection.user.UserFactory;
import com.attitudetech.basicstruct.connection.user.UserSocket;
import com.attitudetech.basicstruct.pojo.User;

import org.reactivestreams.Subscriber;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Flowable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subscribers.DefaultSubscriber;
import io.reactivex.subscribers.DisposableSubscriber;

/**
 * Created by phrc on 25/01/17.
 */

public class UserDataManager {

    List<DisposableSubscriber> disposableSubscribers;

    public UserDataManager() {
        disposableSubscribers = new ArrayList<>();
    }

    public void getUserAPI(String userName, DefaultSubscriber<User> userObserver) {
        IUser userRequest = UserFactory.getUserProxy(UserApi.class.getName());

        userRequest.user(userName)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(userObserver);

    }

    public void getUserSocket(String userName, DisposableSubscriber<User> userObserver) {
        IUser userRequest = UserFactory.getUserProxy(UserSocket.class.getName());
        disposableSubscribers.add(userObserver);

        userRequest.user(userName)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(userObserver);
    }

    public void getUser(String userName, DisposableSubscriber<User> userObserver){
        IUser userRequest = UserFactory.getUserProxy(UserSocket.class.getName());

        disposableSubscribers.add(userObserver);

        userRequest
                .user(userName)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(userObserver);

    }

    public void close(){
        for (DisposableSubscriber disposableSubscriber: disposableSubscribers) {
            disposableSubscriber.dispose();
        }
    }


}
