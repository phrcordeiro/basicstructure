package com.attitudetech.basicstruct.connection.user;

import com.attitudetech.basicstruct.pojo.User;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import retrofit2.http.Path;

/**
 * Created by phrc on 2/7/17.
 */

public interface IUser {

    Flowable user(String user);

}
