package com.attitudetech.basicstruct.connection.user;

import com.attitudetech.basicstruct.connection.socketio.SocketListener;
import com.attitudetech.basicstruct.connection.socketio.SocketManager;
import com.attitudetech.basicstruct.pojo.User;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.FlowableEmitter;
import io.reactivex.FlowableOnSubscribe;
import io.reactivex.Observable;
import io.reactivex.functions.Action;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;
import io.socket.emitter.Emitter;

/**
 * Created by phrc on 2/7/17.
 */

public class UserSocket implements IUser{

    @Override
    public Flowable user(String userName){
        return SocketManager.instance().on(userName, new SocketListener<User>());
    }


    public Flowable userMike(String userName){
        return SocketManager.instance().on(userName, new SocketListener<User>());
    }
}

/*
return Flowable
                .create((FlowableEmitter<User> emmiter) -> {
                    userListener = new Emitter.Listener() {
                        @Override
                        public void call(Object... args) {
                            User user1 = new User();
                            emmiter.onNext(user1);
                        }
                    };
                    SocketManager.instance().on(userName, userListener).connect();
                }, BackpressureStrategy.BUFFER)
                .doOnCancel(new Action() {
                    @Override
                    public void run() throws Exception {
                        SocketManager.instance().off(userName, userListener);
                    }
                });
 */