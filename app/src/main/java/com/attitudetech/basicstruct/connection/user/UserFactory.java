package com.attitudetech.basicstruct.connection.user;

/**
 * Created by phrc on 2/7/17.
 */

public class UserFactory {

    public static IUser getUserProxy(String type){
        if(type == UserApi.class.getName()){
            return new UserApi();
        }else if (type == UserSocket.class.getName()){
            return new UserSocket();
        }
        return null;
    }
}
